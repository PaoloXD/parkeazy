<?php




/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/',function(){
   return 'BACKEND';
});

Route::get('/test','ReportController@test');
Route::group(['middleware'=>['cors'],'prefix' => 'api/v1'],function (){

    Route::post('/authenticate','AuthController@auth_login');
    Route::get('/parking/filter','ParkingController@filter');
    Route::get('/parking/{id?}','ParkingController@get');
    Route::post('/register','UserController@register');

   Route::group(['middleware'=>['jwt.auth']],function (){
        Route::get('/company/{companyId?}','CompanyController@get');
        Route::get('/company/{companyId}/users','CompanyController@get_users');
        Route::post('/company','CompanyController@create');
        Route::delete('/company/{companyId}','CompanyController@delete');
        Route::patch('/company/{companyId}','CompanyController@update');
        Route::patch('/assign','CompanyController@attachUser');
        Route::delete('/unassign','CompanyController@unattachUser');
        Route::post('/branch/{companyId}','ParkingController@create');

        Route::get('/branch/{id}','ParkingController@settings');
        Route::patch('/branch/{id}','ParkingController@Update_settings');
        Route::delete('/parking/{id}','ParkingController@delete');
            Route::get('/user/{user_id?}','UserController@get');
            Route::get('/user/{userType}/list','UserController@unassign');
            Route::post('/user','UserController@create');
            Route::delete('/user/{id}','UserController@deleteUser');
            Route::patch('/user/{id}','AuthController@updateUserById');
            Route::get('/user/{reservation_code}/validate','UserController@ReservationValidate');
                Route::get('/profile','AuthController@getAuthenticatedUser');
                Route::put('/profile','AuthController@updateUserDetails');

        Route::post('/reserve/{parkingId}','ReservationController@create')->middleware(['reservation']); //reservation route
        Route::get('/reserve/{parkingId}','ReservationController@PreparePayment')->middleware(['reservation']);
        //Route::get('/parking/{name}','ParkingController@getSettingByName');


       Route::get('/branches/{branchId?}','CompanyController@getCompanyBranch');
       Route::get('/branches/{branchId}/{userType}','CompanyController@getUsersOnBranchById');
       Route::get('/supervisor','CompanyController@getAssignBranchByLoggedInUser');


        Route::group(['prefix' => 'cashier'],function (){
            Route::get('/profile','CashierController@getSettings');
            Route::get('/reservation/{reservation_code}/validate','CashierController@ReservationValidate');
            Route::post('/reservation/in','CashierController@ClockInReservation');
            Route::post('/reservation/out','CashierController@ClockOutReservation');
            Route::get('/reports','ReportController@generateCashierReport');
        });

        Route::group(['prefix' => 'report'],function (){
            Route::get('/company/{id?}','ReportController@CompanyReport');
            Route::get('/company/{company_id}/{parking_id}','ReportController@CompanyParkingReport');
            Route::get('/company/{company_id}/{parking_id}/{cashier_id}','ReportController@CompanyCashierReport');
            Route::get('/cashier/{id?}','ReportController@generateCashierReport');
            //Route::get('/company/{id}/user/{userId}','ReportController@CompanyUserReport');
        });
   });
});
