<?php

namespace App\Http\Controllers;

use App\Company;
use App\Reservation;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Response;
use App\Services\OneSignal as OneSignal;

class ReportController extends Controller
{
    public function CompanyReport(Request $request,$id = null)
    {

        if(isset($id))
        {
            return $this->getCompany($id);
        }

        return $this->getCompany();
    }

    public function CompanyUserReport($id,$userId)
    {

    }

    public function getCompany($id = null)
    {
        if(isset($id))
        {
            $list = Company::where('companyId',$id)->with([
                'companyParking' => function($q){
                    $q->select('*')->with([
                        'parkingDetail' => function($q){
                            $q->select('*');
                        },'reservation' => function($q){
                            $q->select('*');
                            $q->with(['transactionTo' => function($q){
                                $q->select('*');
                            }]);
                        }
                    ]);
                }
            ])->get();

            return response()->json([
                'result' => 'success',
                'data' => $list
            ],200);
        }

        $list = Company::with([
            'companyParking' => function($q){
                $q->select('*')->with([
                    'parkingDetail' => function($q){
                        $q->select('*');
                    },'reservation' => function($q){
                        $q->select('*')->with([
                            'transactionTo' => function($q){
                                $q->select('*');
                            }
                        ]);
                    }
                ]);
            }
        ])->get();

        return response()->json([
            'result' => 'success',
            'data' => $list
        ],200);
    }

    public function generateCashierReport(){
        $user = new AuthController();
        $userId = json_decode($user->getAuthenticatedUser()->content())->id;
        if(isset($id))
        {
            $userId = intval($id);
        }

        $report = Transaction::where('cashier_id',$userId)->with(['reservation' => function($q){
            $q->select('*');
        }])->get()->toArray();

        return response()->json([
            'result' => 'success',
            'data' => $report
        ],200);

    }

    public function CompanyParkingReport($company_id,$parking_id)
    {
        $report = Company::where('companyId',$company_id)->with([
            'companyParking' => function($q) use ($company_id,$parking_id){
                $q->select('*')->with([
                    'parkingDetail' => function($q) use ($parking_id){
                        $q->select('*')->where('parkingId',$parking_id);
                    },'reservation' => function($q){
                        $q->select('*')->with([
                            'transactionTo' => function($q){
                                $q->select('*');
                            }
                        ]);
                    }
                ]);
            }
        ])->get();

        /*Reservation::where('parkingId',$parking_id)->with(['transactionTo' => function($q){
            $q->select('*');
        }])->get()->toArray();*/

        return response()->json([
            'result' => 'success',
            'data' => $report
        ],200);
    }

    public function CompanyCashierReport($company_id,$parking_id,$cashier_id){
        $report = Company::where('companyId',$company_id)->with([
            'companyParking' => function($q) use ($company_id,$parking_id,$cashier_id){
                $q->select('*')->with([
                    'parkingDetail' => function($q) use ($parking_id,$cashier_id){
                        $q->select('*')->where('parkingId',$parking_id);
                    },'reservation' => function($q) use ($cashier_id){
                        $q->select('*')->with([
                            'transactionTo' => function($q) use ($cashier_id){
                                $q->select('*')->where('cashier_id',$cashier_id);
                            }
                        ]);
                    }
                ]);
            }
        ])->get();

        /*Reservation::where('parkingId',$parking_id)->with(['transactionTo' => function($q){
            $q->select('*');
        }])->get()->toArray();*/

        return response()->json([
            'result' => 'success',
            'data' => $report
        ],200);
    }
}
