<?php

namespace App\Http\Controllers;

use App\ParkingMap;
use App\User;
use App\UserPreviledge;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Company;
use Illuminate\Support\Facades\DB;
use Response;
use App\CompanyParking;
use JWTAuth;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->userType = [
            '0' => 'SuperAdmin',
            '1' => 'Admin',
            '2' => 'Supervisor',
            '3' => 'Cashier',
            '4' => 'Mobile'
        ];
    }

    public function get($companyId = null)
    {
        if(isset($companyId))
        {
            $data = Company::with([
                'companyParking' => function($query){
                    $query->select('*')->with([
                        'parkingDetail' => function($query){
                            $query->select('*');
                        }]);
                }
            ])->where('companyId',$companyId)->first();


            return response()->json([
                'result' => 'success',
                'data' => $data
            ],200);
        }

        $dataQuery = Company::all();

        $data = $dataQuery;
        return response()->json([
            'result' => 'success',
            'data' => $data
        ],200);
    }

    public function create(Request $request)
    {
        if(Company::create($request->all()))
        {
            return response()->json([
                'result' => 'success',
                'code' => 200
            ],200);
        }

        return response()->json([
            'result' => 'success',
            'code' => 401
        ],401);
    }

    public function update(Request $request,$companyId)
    {
        $data = $request->only('companyAddress','companyName','companyStatus');

        if(Company::where('companyId',$companyId)->update($data))
        {
            return response()->json([
                'result' => 'success',
                'code' => 200
            ],200);
        }

        return response()->json([
            'result' => 'failed',
            'code' => 401
        ],401);
    }

    /**
     * @param Request $request
     * Attanch user to a company or parking booth initial
     * @return mixed
     */
    public function attachUser(Request $request)
    {
        if($request->has('companyId'))
        {
            $companyId = $request->companyId;
            if(UserPreviledge::where('userId',$request->userId)->update(['companyId' => $companyId]))
            {
                return response()->json([
                    'result' => 'success'
                ],200);
            }
        }

        if($request->has('parkingId'))
        {
            $parkingId = $request->parkingId;
            $company = CompanyParking::where('parkingId',$parkingId)->first();
            if(UserPreviledge::where('userId',$request->userId)->update(['parkingId' => $parkingId,
                'companyId' => $company->companyId]))
            {
                return response()->json([
                    'result' => 'success'
                ],200);
            }
        }

        return response()->json([
            'result' => 'failed'
        ],401);
    }

    public function unattachUser(Request $request){
        if($request->has('companyId')){
            $queryData = $request->only('userId','companyId');
            $isRemoved = UserPreviledge::where($queryData)->update(['companyId' => 0]);
            if($isRemoved){
                return response()->json([
                    'result' => 'success'
                ]);
            }
        }

        if($request->has('parkingId')){
            $queryData = $request->only('userId','parkingId');
            $isRemoved = UserPreviledge::where($queryData)->update(['parkingId' => 0]);
            if($isRemoved){
                return response()->json([
                    'result' => 'success'
                ]);
            }
        }
        return response()->json([
            'result' => 'failed'
        ]);
    }

    public function get_users(Request $request,$companyId)
    {
        $users = [];
        $lists = UserPreviledge::where('companyId',$companyId)->get();

        if(count($lists) != 0)
        {
            foreach ($lists as $list)
            {
                $queryDetails = User::where(['id' => $list->userId, 'userType' => $request->type])->with([
                    'userPreviledge'=>function($query){
                        $query->select('*')->with(['companyParking'=>function($query){
                            $query->select('*')->with(['company'=>function($query){
                                $query->select('*');
                            }]);
                        }]);
                    },'userDetails' => function($query){
                        $query->select('*');
                    }])->first();

                if($queryDetails != null)
                {
                    array_push($users,$queryDetails->toArray());
                }

            }

            return response()->json([
                'result' => 'success',
                'data' => $users
            ],200);
        }

        return response()->json([
            'result' => 'failed',
            'msg' => 'Not found.'
        ],200);

    }

    public function delete($id)
    {
        $isDeleted = DB::table('company')->select('company.*','companyparking.*','parkingdetails ')
                    ->leftJoin('companyparking','company.companyId','=','companyparking.companyId')
                    ->leftJoin('parkingdetails','companyparking.parkingId','=','parkingdetails.parkingId')
                    ->where('company.companyId','=',$id)->delete();

        if($isDeleted){
            return response()->json([
                'result' => 'success',
                'msg' => 'Company deleted.'
            ],200);
        }

        return response()->json([
            'result' => 'failed',
            'msg' => 'Not found.'
        ],404);
    }

    public function getCompanyBranch($branchId = null){
        $user = JWTAuth::parseToken()->authenticate()->toArray();
        $companyId = UserPreviledge::where('userId',$user['id'])->get()->first()->companyId;

        $data = UserPreviledge::distinct()->where('companyId',$companyId)->with([
            'parkingDetails' => function($q){
                $q->select('*');
            }
        ])->groupBy('parkingId')->get();

        if(isset($branchId))
        {
            $data = UserPreviledge::distinct()->where('companyId',$companyId)
                    ->with([
                        'parkingDetails' => function($q){
                            $q->select('*');
                        },'users' => function($q){
                            $q->select('*');
                        }
                    ])->where('parkingId',$branchId)->groupBy('parkingId')->get();
        }

        return response()->json([
            'result' => 'success',
            'data' => $data
        ]);
    }

    public function getAssignBranchByLoggedInUser()
    {
        try{
            $user = JWTAuth::parseToken()->authenticate()->toArray();
            $data = UserPreviledge::where('userId',$user['id'])->with([
                'companyParking' => function($q){
                    $q->select('*');
                }
            ])->get();

            if($data->company_parking == null){
                return response()->json([
                    'result' => 'failed',
                    'msg' => 'Please contact admin'
                ],401);
            }

            return response()->json([
                'result' => 'success',
                'data' => $data
            ]);
        }catch(\Exception $e){
            return response()->json([
                'result' => 'failed',
                'msg' => 'Please contact admin'
            ],401);
        }

    }

    public function getCompanyBranchAndUserByBranchId($branchId)
    {
        $user = JWTAuth::parseToken()->authenticate()->toArray();
        $companyId = UserPreviledge::where('userId',$user['id'])->get()->first()->companyId;
        $users = DB::table('userpreviledge')->select('userpreviledge.*','credentials.*')
            ->join('credentials','userpreviledge.userId','=','credentials.id')
            ->where('userpreviledge.companyId','=',$companyId)
            ->where('userpreviledge.parkingId','=',$branchId)
            ->where('credentials.usertype','=',2)->get();

        return response()->json([
            'result' => 'success',
            'data' => $users
        ]);
    }

    public function getUsersOnBranchById($branchId,$userType)
    {
        $user = JWTAuth::parseToken()->authenticate()->toArray();
        $companyId = UserPreviledge::where('userId',$user['id'])->get()->first()->companyId;

        $data = DB::table('userpreviledge')->select('credentials.*','userdetails.*')
            ->join('credentials','userpreviledge.userId','=','credentials.id')
            ->join('userdetails','userpreviledge.userId','=','userdetails.detailsId')
            ->where('userpreviledge.companyId','=',$companyId)
            ->where('userpreviledge.parkingId','=',$branchId)
            ->where('credentials.usertype','=',$userType)->get();

        return response()->json([
            'result' => 'success',
            'data' => $data
        ]);
    }
}
