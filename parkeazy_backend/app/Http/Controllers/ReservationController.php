<?php

namespace App\Http\Controllers;

use App\ParkingDetails;
use App\QRcode;
use App\Reservation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;


class ReservationController extends Controller
{
    public function create(Request $request,$parkingId)
    {
        DB::beginTransaction();
        try{

            $user = JWTAuth::parseToken()->authenticate();
            $reservationDetails = $request->only('start','end','plateNumber','brand','model','make','color');
            $reservationDetails['user_id'] = $user->id;
            $reservationDetails['start'] = Carbon::parse($request->start)->toTimeString();
            $reservationDetails['end'] = Carbon::parse($request->end)->toTimeString();
            $reservationDetails['parkingId'] = intval($parkingId);
            $reservationDetails['date'] = Carbon::now('Asia/Singapore')->toDateString();
                $payment = $this->generateReservationAmount($parkingId,$reservationDetails['start'],$reservationDetails['end']);
            $reservationDetails['reservation_fee'] = $payment;
                $reservation_id = Reservation::create($reservationDetails)->id;
            //dd($reservationDetails);
            $qr = json_decode($this->QrCode()->content());
            //dd($qr);
                if($qr->result != 'success')
                {
                    DB::rollback();
                    return response()->json([
                        'result' => 'failed',
                        'msg' => 'Reservation error .'
                    ],400);
                }

            $qr_data = array(
                'qr_code' => $qr->qrcode,
                'status' => 0,
                'reservation_id' => $reservation_id,
                'reservation_code' => $qr->code
            );

            $QR = QRcode::create($qr_data);
            $parkingUpdate = true; //ParkingDetails::where('parkingId',$parkingId)->decrement('availableSlots',1);
            if($QR && $parkingUpdate)
            {
                DB::commit();
                return response()->json([
                    'result' => 'success',
                    'msg' => 'Reservation Success.',
                    'data' => $qr_data
                ],200);
            }


        }catch (\Exception $e)
        {
            DB::rollback();
            return response()->json([
                'result' => 'failed',
                'msg' => 'Reservation error.',
                'debug' => $e
            ],400);
        }
    }

    /**
     * Create payment here
     * generate QRcode
     */
    public function QrCode()
    {
        $code = str_random(6);
        try{
            $qr_filename = Carbon::now()->timestamp.'.png';
            $renderer = new \BaconQrCode\Renderer\Image\Png();
            $renderer->setHeight(350);
            $renderer->setWidth(350);
            $writer = new \BaconQrCode\Writer($renderer);
            $writer->writeFile($code, public_path('QRcodes/'.$qr_filename));
        }catch (\Exception $e) {
            return response()->json([
                'result' => 'failed',
                'msg' => 'Generating QR code.',
                'debug' => $e
            ],400);
        }

        return response()->json([
            'result' => 'success',
            'qrcode' => $qr_filename,
            'code' => $code
        ],200);

    }

    public function PreparePayment(Request $request,$parkingId)
    {
        try {
            $available = ParkingDetails::where('parkingId', $parkingId)->first();

            if ($available == null || $available->availableSlots == 0) {
                return response()->json([
                    'result' => 'failed',
                    'msg' => 'Parking not found.'
                ], 403);
            }

            $user = JWTAuth::parseToken()->authenticate();
            $reservationDetails = $request->only('start','end','plateNumber', 'brand', 'model', 'make', 'color');
            $reservationDetails['user_id'] = $user->id;
            $reservationDetails['start'] = Carbon::parse($request->start)->toTimeString();
            $reservationDetails['end'] = Carbon::parse($request->end)->toTimeString();
            $reservationDetails['parkingId'] = intval($parkingId);


                if(Carbon::parse($reservationDetails['start']) >= Carbon::parse($reservationDetails['end']))
                {
                    $reservationDetails['end'] = Carbon::parse($reservationDetails['end'])->addHours(24);
                }

                if(Carbon::parse($reservationDetails['start']) <= Carbon::parse($reservationDetails['end']))
                {
                    $reservationDetails['end'] = Carbon::parse($reservationDetails['end']);
                }

            $reserveTime = Carbon::parse($reservationDetails['end'])->diffInMinutes(Carbon::parse($reservationDetails['start']));

            if($reserveTime < $available->reservation_min_time)
            {
                return response()->json([
                    'result' => 'failed',
                    'data' => $reservationDetails,
                    'msg' => 'Minimum time not met.'
                ],200);
            }

            $ExtraHour = $reserveTime - $available->reservation_min_time;
            $perHourAmount = ($ExtraHour / 60) * $available->amount_per_hour;
            $TotalPayment = $available->reservation_amount + $perHourAmount;

            return response()->json([
                'result' => 'success',
                'data' => array(
                        'details' => $reservationDetails,
                        'payment' => $TotalPayment
                ),
                'msg' => 'Ready for Payment.'
            ],200);
        } catch (\Exception $e){
            return $e;
        }
    }

    public function generateReservationAmount($parkingId,$startTime,$endTime){
        $available = ParkingDetails::where('parkingId',$parkingId)->first();

        if(Carbon::parse($startTime) >= Carbon::parse($endTime))
        {
            $endTime = Carbon::parse($endTime)->addHours(24);
        }

        if(Carbon::parse($startTime) <= Carbon::parse($endTime))
        {
            $endTime = Carbon::parse($endTime);
        }

        $reserveTime = Carbon::parse($endTime)->diffInMinutes(Carbon::parse($startTime));
        $TotalPayment = 0;

        $ExtraHour = $reserveTime - $available->reservation_min_time;
        $perHourAmount = ($ExtraHour / 60) * $available->amount_per_hour;
        $TotalPayment = $available->reservation_amount + $perHourAmount;


        return round($TotalPayment, 2);
    }
}
