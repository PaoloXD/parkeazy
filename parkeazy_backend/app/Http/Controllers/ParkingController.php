<?php

namespace App\Http\Controllers;


use App\ParkingDetails;
use App\UserPreviledge;
use Illuminate\Http\Request;
use App\CompanyParking;
use Response;
use DB;

class ParkingController extends Controller
{
    public function get($id = null)
    {
        $data = ParkingDetails::all();
        if(isset($id))
        {
            if(is_numeric($id))
            {
                $data = ParkingDetails::where('parkingId',$id)->first();

                return response()->json([
                    'result' => 'success',
                    'data' => $data,
                    'code' => 200
                ],200);

            }else{
                $data = ParkingDetails::where('parkingName',$id)->first();
                $data->reservation_min_time = $data->reservation_min_time / 60;
                return response()->json([
                    'result' => 'success',
                    'data' => $data,
                    'code' => 200
                ],200);
            }

        }


        return response()->json([
            'result' => 'success',
            'data' => $data,
            'code' => 200
        ],200);
    }

    public function create($companyId,Request $request)
    {
        /* FIXED!
         * This is need to fix for performance improvement
         * */
        DB::beginTransaction();

        $details = $request->only('parkingName','address','numberOfSlots','lat','lng');
        $details['availableSlots'] = $details['numberOfSlots'];
        //dd($details);

        try{
            $parkingId = CompanyParking::create(['companyId' => $companyId])->id;
        }catch (\Exception $e)
        {
            DB::rollback();
            return response()->json([
                'result' => 'failed'
            ],401);
        }

        try{
            $details['parkingId'] = $parkingId;
            ParkingDetails::create($details);
        }catch (\Exception $e)
        {
            DB::rollback();
            return response()->json([
                'result' => 'failed'
            ],401);
        }

        DB::commit();
        return response()->json([
            'result' => 'success'
        ],200);
    }

    public function settings($id)
    {
        $branch = ParkingDetails::where('parkingId',$id)->first();
        if($branch != null)
        {
            $users = $this->getBranchUsers($id);

            return response()->json([
                'result' => 'success',
                'data' => array(
                    'settings' => $branch,
                    'users' => $users
                )
            ],200);
        }

        return response()->json([
            'result' => 'failed',
            'msg' => 'Not found'
        ],200);
    }

    public function Update_settings(Request $request, $id)
    {
        if($request->availableSlots > $request->numberOfSlots){
            return response()->json([
                'result' => 'failed',
                'msg' => 'Not allowed'
            ],500);
        }

        $data = $request->only('numberOfSlots','reservation_amount','reservation_min_time','succeeding_amount','availableSlots','succeeding_time','amount_per_hour');
        $update = ParkingDetails::where('parkingId',$id)->update($data);
        if($update)
        {
            return response()->json([
                'result' => 'success',
                'msg' => 'Settings updated'
            ],200);
        }

        return response()->json([
            'result' => 'failed',
            'msg' => 'Update failed'
        ],500);
    }

    public function delete($id){
        $isDeleted = CompanyParking::where('parkingId',$id)->delete();
        $isRemoved = ParkingDetails::where('parkingId',$id)->delete();
        if($isRemoved && $isDeleted){
            return response()->json([
                'result' => 'success',
                'msg' => 'Parking removed'
            ]);
        }

        return response()->json([
            'result' => 'failed',
            'msg' => 'Failed to remove.'
        ]);
    }

    public function getBranchUsers($branch_id)
    {
        return DB::table('userpreviledge')->select('userpreviledge.*','credentials.*','userdetails.*') //
            ->join('credentials','userpreviledge.userId','=','credentials.id')
            ->join('userdetails','credentials.id','=','userdetails.detailsId')
            ->where('userpreviledge.parkingId','=',$branch_id)->get();
    }

    public function getSettingByName($name)
    {
        dd($name);
    }

    public function filter(Request $request)
    {
        $lat = $request->lat;
        $lng = $request->lng;
        $radius = 3963.17 ;
        $data = ParkingDetails::
            select(DB::raw("*, " .
                "( " .
                "6371 * " .
                "acos( " .
                "cos( radians(?) ) * " .
                "cos( radians( lat ) ) * " .
                "cos( " .
                "radians( lng ) - radians(?)" .
                ") + " .
                "sin( radians(?) ) * " .
                "sin( radians( lat ) ) " .
                ")" .
                ") AS distance"))
                ->having("distance", "<", $radius)
                ->orderBy("distance")
                ->setBindings([ $lat, $lng, $lat ], 'select')->get();

        return response()->json([
            'result' => 'success',
            'data' => $data
        ],200);
    }
}
