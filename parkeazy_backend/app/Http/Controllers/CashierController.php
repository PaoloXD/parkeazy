<?php

namespace App\Http\Controllers;

use App\ParkingDetails;
use App\QRcode;
use App\Transaction;
use App\UserPreviledge;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Response;

class CashierController extends Controller
{
    /*
     * Client time in to parking locations
     * Param
     * - reservation_code , current_time
     * - current system time
     * */
    public function ReservationValidate(Request $request,$reservation_code)
    {
        try {
            $auth = new AuthController();
            $user_id = json_decode($auth->getAuthenticatedUser()->content())->id;
            $cashier = UserPreviledge::where('userId', $user_id)->first();
            $reservation = $this->SearchCode($reservation_code, $cashier->parkingId);

            $transaction = Transaction::where('reservation_id', $reservation->reservation_id)->first();

            if ($reservation != null) {
                if ($transaction == null) {
                    $action = 'time_in';
                    return response()->json([
                        'result' => 'success',
                        'data' => $reservation,
                        'action' => $action
                    ], 200);
                }

                if ($transaction != null) {
                    $action = 'time_out';
                    $payout = json_decode($this->TimeOutGetOverTime($cashier->parkingId, $reservation));

                    return response()->json([
                        'result' => 'success',
                        'data' => $reservation,
                        'payouts' => $payout,
                        'action' => $action
                    ], 200);
                }

            }
        }catch (\Exception $e){
            return response()->json([
                'result' => 'failed',
                'msg' => 'Code maybe invalid or not available in this parking.'
            ], 200);
        }
    }

    public function SearchCode($code = null,$parkingId,$status = null, $timeRange = array())
    {
        if(isset($code))
        {
            return DB::table('qr')->select('qr.*','reservation.*')
                ->join('reservation','qr.reservation_id','=','reservation.id')
                ->where('qr.reservation_code','=',$code)
                //->where('qr.status',$status)
                ->where('reservation.parkingId','=',$parkingId)
                ->first();
        }

        if(isset($status))
        {
            return DB::table('qr')->select('qr.*','reservation.*')
                ->join('reservation','qr.reservation_id','=','reservation.id')
                //->orWhere('qr.reservation_code',$code)
                ->where('qr.status',$status)
                ->where('reservation.parkingId',$parkingId)
                //->where('reservation.start','>=',$timeRange['start'])
                //->where('reservation.end','<=',$timeRange['end'])
                ->whereBetween('reservation.start', $timeRange)
                //->whereBetween('reservation.end', $timeRange)
                ->get();
        }

    }

    /*
     * Client time in to parking locations
     * Param
     * - 'reservation_id', 'current_time','parkingId'
     * - current system time
     * */
    public function ClockInReservation(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = new AuthController();
            $cashier = json_decode($user->getAuthenticatedUser()->content());

            $transaction = $request->only('reservation_id', 'parkingId');
            $transaction['cashier_id'] = $cashier->id;
            $transaction['actual_in'] = Carbon::parse($request->current_time)->toTimeString();

            $CheckExisting = Transaction::where('reservation_id',intval($transaction['reservation_id']))->first();

            if($CheckExisting != null)
            {
                return response()->json([
                    'result' => 'failed',
                    'msg' => 'Already time in'
                ],200);
            }
                $transac = Transaction::create($transaction);
                //$parkingUpdate = ParkingDetails::where('parkingId',$request->parkingId)->decrement('availableSlots',1);

            if($transac)
            {
                ParkingDetails::where('parkingId',intval($request->parkingId))->decrement('availableSlots',1);
                DB::commit();
                return response()->json([
                    'result' => 'success',
                    'msg' => 'Customer successfully time in.'
                ],200);
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'result' => 'failed',
                'msg' => 'Timing in problem. Please try again.'
            ],200);
        }
    }


    /*
     * Client time in to parking locations
     * Param
     * - 'reservation_id', 'actual_out','parkingId','payment','overtime'
     * - current system time
     * */
    public function ClockOutReservation(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = new AuthController();
            $cashier = json_decode($user->getAuthenticatedUser()->content());

            $transaction = $request->only('actual_out','payment','overtime');
            $transaction['cashier_id'] = $cashier->id;
            $transaction['actual_out'] = Carbon::parse($request->actual_out)->toTimeString();

            $transac = Transaction::where('reservation_id',intval($request->reservation_id))->update($transaction);
            $parkingUpdate = ParkingDetails::where('parkingId',intval($request->parkingId))->increment('availableSlots',1);
            $QR_details = QRcode::where('reservation_id',intval($request->reservation_id))->update(['status' => 1]);

            if($transac && $parkingUpdate && $QR_details)
            {
                DB::commit();
                return response()->json([
                    'result' => 'success',
                    'msg' => 'Customer successfully time out.'
                ],200);
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'result' => 'failed',
                'msg' => 'Timing out problem. Please try again.',
                'debug' => $e
            ],200);
        }
    }

    public function TimeOutGetOverTime($parkingId,$reservation)
    {
        $current_time = Carbon::now();
        $parkingDetails = ParkingDetails::where('parkingId',$parkingId)->first();
        $end = Carbon::parse($reservation->end);
        $start = Carbon::parse($reservation->start);
            $duration = $start->diffInMinutes($end);
            //$timeOut = $start->diffInMinutes($current_time);

        $timeOut = Carbon::parse($reservation->created_at)->diffInMinutes(Carbon::parse($current_time));
        /* reservation-created_at - current_time
         * var difference= created_at- current date and time
         * var ot = difference - 
         *
         * */

        $overTime = 0;
        $overTime_amount = 0;

            if($timeOut > $duration)
            {
                $overTime = $timeOut - $duration;
                $overTime_amount = round( ( $overTime / $parkingDetails->succeeding_time) * $parkingDetails->succeeding_amount);
            }

        return json_encode(array(
            'amount' => $overTime_amount,
            'overtime' => $overTime,
            'time_out' => $current_time->toTimeString()
        ));

    }

    public function getSettings()
    {
        try {
            $user = new AuthController();
            $cashier = json_decode($user->getAuthenticatedUser()->content());
            $settings = DB::table('userpreviledge')->select('userpreviledge.*', 'parkingdetails.*')
                ->join('parkingdetails', 'userpreviledge.parkingId', '=', 'parkingdetails.parkingId')
                ->where('userpreviledge.userId', '=', $cashier->id)
                ->first();

            return response()->json(array(
                'result' => 'success',
                'data' => $settings
            ));
        } catch (\Exception $e) {
            return response()->json(array(
                'result' => 'failed',
                'msg' => 'Check if user is already assign.'
            ));
        }
    }
}
