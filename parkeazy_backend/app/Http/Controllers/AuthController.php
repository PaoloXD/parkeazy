<?php

namespace App\Http\Controllers;

use App\Company;
use App\UserDetails;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;


class AuthController extends Controller
{
    private $userType = [];
    public function __construct()
    {
        $this->userType = [
            '0' => 'SuperAdmin',
            '1' => 'Admin',
            '2' => 'Supervisor',
            '3' => 'Cashier',
            '4' => 'Mobile'
        ];
    }

    public function auth_login(Request $request)
    {
        try {
            if (! $token = JWTAuth::attempt(['email'=>$request->email, 'password'=>$request->password,'userStatus'=>0])) {

                return response()->json([
                    'msg' => 'Invalid email or password',
                    'result' => 'failed',
                    'code' => 401
                ], 401);
            }

            $user = $this->getUser($request->email);
            if($user->userType == 0)
            {
                return response()->json([
                    'token' => $token,
                    'redirect' => $this->userType[$user->userType],
                    'data' => $user,
                    'result' => 'success'
                ]);
            }

            if($user->userType == 1 || $user->userType == 2)
            {
                //for user visor check company status
                //return $user->userpreviledge->companyId;
                $companyStatus = $this->checkCompanyStatus($user->userpreviledge->companyId);
                //dd();
                if($companyStatus['result'] == 'success')
                {
                    return response()->json([
                        'token' => $token,
                        'redirect' => $this->userType[$user->userType],
                        'data' => $user->userpreviledge->toArray(),
                        'result' => 'success'
                    ]);
                }
                return $companyStatus;
            }

            if($user->userType == 3)
            {
                return response()->json([
                    'token' => $token,
                    'redirect' => $this->userType[$user->userType],
                    'data' => $user->userpreviledge->toArray(),
                    'result' => 'success'
                ]);
            }

            if($user->userType == 4)
            {
                return response()->json([
                    'token' => $token,
                    'redirect' => $this->userType[$user->userType],
                    'data' => $user->toArray(),
                    'result' => 'success'
                ]);
            }

        } catch (JWTException $e) {
            // something went wrong
            dd($e);
            return response()->json(['result' => 'could_not_create_token'], 500);
        }


    }

    public function getUser($email)
    {
         return User::select('*')->with(['userPreviledge'=>function($query){
            $query->select('*')->with(['companyParking'=>function($query){
                $query->select('*')->with(['company'=>function($query){
                    $query->select('*');
                }]);
            }]);
        }])->where('email',$email)->first();

    }

    public function checkCompanyStatus($companyId)
    {
        $company = Company::where('companyId',$companyId)->first();
        if($company->companyStatus == 1)
        {
            return array('result' => 'failed',
                'msg' => 'Company is disabled',
                'code' => 401
            );
        }

        return array(
            'result' => 'success',
            'msg' => 'Company Access Granted',
            'code' => 200
        );
    }

    public function logout(Request $request) {
        $this->validate($request, [
            'token' => 'required'
        ]);

        JWTAuth::invalidate($request->input('token'));
    }

    public function getAuthenticatedUser()
    {

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }


        return response()->json(
            User::where('id',$user->id)->with([
                'userDetails' => function($q){
                    $q->select('*');
                }
            ])->first()
        );
    }

    public function updateUserDetails(Request $request)
    {
        DB::beginTransaction();
        try{
            $user = json_decode($this->getAuthenticatedUser()->content());

            $profile = $request->only('firstname','lastname','contact');
            if($request->has('password') && !empty($request->password))
            {
                $password = Hash::make($request->password);
                User::where('id',$user->id)->update(['password' => $password]);
            }

            if(UserDetails::where('detailsId',$user->id)->update($profile))
            {
                DB::commit();
                return response()->json([
                    'result' => 'success',
                    'msg' => 'Profile Updated'
                ],200);
            }
        }catch (\Exception $e){
            DB::rollback();
            return response()->json([
                'result' => 'failed',
                'msg' => 'Profile Updating failed.'
            ],200);
        }
    }

    public function updateUserById(Request $request,$id){
        DB::beginTransaction();
        try{
            $profile = $request->only('firstname','lastname','contact');
            if($request->has('password'))
            {
                $password = Hash::make($request->password);
                User::where('id',$id)->update(['password' => $password]);
            }

            if(UserDetails::where('detailsId',$id)->update($profile))
            {
                DB::commit();
                return response()->json([
                    'result' => 'success',
                    'msg' => 'Profile Updated'
                ],200);
            }
        }catch (\Exception $e){
            DB::rollback();
            return response()->json([
                'result' => 'failed',
                'msg' => 'Profile Updating failed.'
            ],200);
        }
    }

}
