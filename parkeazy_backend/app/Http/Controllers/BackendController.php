<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Http\Requests;
use App\Company;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\UserController;

class BackendController extends Controller
{
    protected $company;
    protected $user;
    protected $auth;

    public function __construct(CompanyController $company,UserController $user,AuthController $authController)
    {
        $this->company = $company;
        $this->user = $user;
        $this->auth = $authController;
    }

    public function get_company($companyId = null)
    {
        if(isset($companyId))
        {
            $parkingList = $this->company->get($companyId);
            return response()->json([
                'result' => 'success',
                'data' => $parkingList,
                'code' => 200
            ],200);

        }
        
        $companyList = $this->company->get();
        $supervisor = $this->user->get(1,null);
        $data = array(
            'company' => $companyList,
            'supervisor' => $supervisor
        );
        return response()->json([
            'result' => 'success',
            'data' => $data,
            'code' => 200
        ],200);

    }

    public function visor_index($userId,Request $request)
    {
        $user = $this->user->get(null,$userId);
        $locationList = $this->company->get($user['user_previledge']['companyId']);
        return response()->json([
            'result' => 'success',
            'data' => $locationList
        ]);
    }
}
