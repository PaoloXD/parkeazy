<?php

namespace App\Http\Controllers;

use App\ParkingDetails;
use App\Transaction;
use App\UserDetails;
use App\UserPreviledge;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use Response;
use Hash;
use DB;
use JWTAuth;

class UserController extends Controller
{
    public function get(Request $request,$user_id = null)
    {

        if(isset($request->userType))
        {
            $queryDetails = User::where('userType',$request->userType)->with([
                'userDetails' => function($query){
                    $query->select('*');
                }
            ])->get();

            return response()->json([
                'result' => 'success',
                'data' => $queryDetails
            ]);
        }

        if(isset($user_id))
        {
            $queryDetails = User::where('id',$user_id)->with([
                'userPreviledge'=>function($query){
                $query->select('*')->with(['companyParking'=>function($query){
                    $query->select('*')->with(['company'=>function($query){
                        $query->select('*');
                    }]);
                }]);
            },'userDetails' => function($query){
                    $query->select('*');
                }])->first();

            return response()->json([
                'result' => 'success',
                'data' => $queryDetails
            ]);
        }
    }

    public function create(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $request['password'] = Hash::make($request->password);
            if ($user->userType != 0 && $request->userType == 0) {
                return response()->json([
                    'result' => 'failed'
                ], 401);
            }

            $isExist = User::where('email',$request->email)->count();
            if($isExist != 0){
                return response()->json([
                    'result' => 'failed',
                    'message' => 'Email is already registered.'
                ]);
            }

            $create_login = $request->only('email', 'password', 'userType');

            $login = User::create($create_login);
            $userDetail =  UserDetails::create(['detailsId' => $login->id,'firstname' => $request->firstname,
                                                'lastname' => $request->lastname, 'contact' => $request->contact]);
            $userPreviledge = array(
                'userId' => $login->id
            );

            $previledge = UserPreviledge::create($userPreviledge);
            if($login && $previledge && $userDetail)
            {
                DB::commit();
                return response()->json([
                    'result' => 'success',
                    'data' => array('id' => $login->id),
                ],200);
            }

        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'result' => 'failed'
            ],500);
        }

    }

    public function unassign(Request $request,$userType)
    {
        //return array('sample' => 'sample return');
        $users = [];
        $userController = new UserController();
        $lists = UserPreviledge::where('companyId',0)->get();
        if(count($lists) != 0)
        {
            foreach ($lists as $list)
            {
                $filtered = Collect(json_decode($userController->get(new Request(),$list->userId)->content())->data);
                $data = $filtered->get('userType');

                if($data == intval($userType))
                {
                    array_push($users,$filtered);
                }

            }

            /*$filtered_type = Collect($users);
            $filtered_type = $filtered_type->where('userType',intval($userType));*/
            //dd($users);
            return response()->json([
                'result' => 'success',
                'data' => $users
            ],200);
        }
        return response()->json([
            'result' => 'failed'
        ],200);

    }

    public function register(Request $request)
    {
        DB::beginTransaction();

        try {
            $create_login = $request->only('email', 'password','signal_id');
                $isExist = User::where('email',$request->email)->count();
                if($isExist != 0){
                    return response()->json([
                        'result' => 'failed',
                        'message' => 'Email is already registered.'
                    ]);
                }

            $create_login['password'] = Hash::make($request->password);
            $create_login['userType'] = 4;
            $login = User::create($create_login);
            $userDetail =  UserDetails::create(['detailsId' => $login->id]);
            $userPreviledge = array(
                'userId' => $login->id
            );

            $previledge = UserPreviledge::create($userPreviledge);
            if($login && $previledge && $userDetail)
            {
                DB::commit();
                return response()->json([
                    'result' => 'success',
                    'data' => array('id' => $login->id),
                ],200);
            }

        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'result' => 'failed'
            ],500);
        }
    }

    public function deleteUser($id){
        DB::beginTransaction();
        try{
            if(User::where('id',$id)->delete())
            {
                $delete = UserPreviledge::where('userId',$id)->delete();
                $isdeleted = UserDetails::where('detailsId',$id)->delete();
                if($delete && $isdeleted){
                    DB::commit();
                    return response()->json([
                        'result' => 'success'
                    ],200);
                }
            }
        }catch (\Exception $e){
            DB::rollback();
            return response()->json([
                'result' => 'failed'
            ],200);
        }

    }

    public function ReservationValidate($reservation_code)
    {
        try {
            /*$auth = new AuthController();
            $user_id = json_decode($auth->getAuthenticatedUser()->content())->id;
            $cashier = UserPreviledge::where('userId', $user_id)->first();*/
            $reservation = $this->SearchCode($reservation_code);

            $transaction = Transaction::where('reservation_id', $reservation->reservation_id)->first();

            if ($reservation != null) {
                if ($transaction == null) {
                    $action = 'time_in';
                    return response()->json([
                        'result' => 'success',
                        'data' => $reservation,
                        'action' => $action
                    ], 200);
                }

                if ($transaction != null) {
                    $action = 'time_out';
                    $payout = json_decode($this->TimeOutGetOverTime($reservation->parkingId, $reservation));

                    return response()->json([
                        'result' => 'success',
                        'data' => $reservation,
                        'payouts' => $payout,
                        'action' => $action
                    ], 200);
                }

            }
        }catch (\Exception $e){
            return response()->json([
                'result' => 'failed',
                'msg' => 'Code maybe invalid or not available in this parking.'
            ], 200);
        }
    }

    public function SearchCode($code)
    {

        return DB::table('qr')->select('qr.*','reservation.*')
            ->join('reservation','qr.reservation_id','=','reservation.id')
            ->where('qr.reservation_code','=',$code)
            //->where('qr.status',$status)
            ->first();


    }

    public function TimeOutGetOverTime($parkingId,$reservation)
    {
        $current_time = Carbon::now();
        $parkingDetails = ParkingDetails::where('parkingId',$parkingId)->first();
        $end = Carbon::parse($reservation->end);
        $start = Carbon::parse($reservation->start);
        $duration = $start->diffInMinutes($end);
        //$timeOut = $start->diffInMinutes($current_time);

        $timeOut = Carbon::parse($reservation->created_at)->diffInMinutes(Carbon::parse($current_time));
        /* reservation-created_at - current_time
         * var difference= created_at- current date and time
         * var ot = difference -
         *
         * */

        $overTime = 0;
        $overTime_amount = 0;

        if($timeOut > $duration)
        {
            $overTime = $timeOut - $duration;
            $overTime_amount = round( ( $overTime / $parkingDetails->succeeding_time) * $parkingDetails->succeeding_amount);
        }

        return json_encode(array(
            'amount' => $overTime_amount,
            'overtime' => $overTime,
            'time_out' => $current_time->toTimeString()
        ));

    }
}
