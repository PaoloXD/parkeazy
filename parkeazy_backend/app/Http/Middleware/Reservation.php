<?php

namespace App\Http\Middleware;

use App\Http\Controllers\CashierController;
use App\Http\Controllers\ParkingController;
use Carbon\Carbon;
use Closure;
use Response;

class Reservation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        /*if($request->start < Carbon::now()->hour){
            return Response::json([
                'result' => 'failed',
                'msg' => 'Time is not acceptable. Choose time not less than current time'
            ],200);
        }*/

        $id = intval($request->route('parkingId'));
        $Parking = new ParkingController();


        $details = json_decode($Parking->settings($id)->content());

            if($details->result == 'failed')
            {
                return Response::json([
                    'result' => 'failed',
                    'msg' => 'Parking Not found.'
                ],200);
            }

            if($details->data->settings->availableSlots == 0)
            {
                return Response::json([
                    'result' => 'failed',
                    'msg' => 'No available slots'
                ],200);
            }

        return $next($request);
    }
}
