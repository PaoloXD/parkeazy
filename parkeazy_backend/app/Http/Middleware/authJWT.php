<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Response;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\JWTAuth;

class authJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        try {
            $user = JWTAuth::toUser($request->header('token'));
        } catch (Exception $e) {
            if ($e instanceof TokenInvalidException){
                return response()->json([
                    'result'=>'token_failed',
                    'msg' => 'Token is Invalid'
                ],501);
            }else if ($e instanceof TokenExpiredException){
                return response()->json([
                    'result'=>'token_failed',
                    'msg' => 'Token is Expired'
                ],501);
            }else{
                return response()->json([
                    'result'=>'token_failed',
                    'msg' => 'Something is wrong'
                ],501);
            }
        }
        return $next($request);
    }
}
