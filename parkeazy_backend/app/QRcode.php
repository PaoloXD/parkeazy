<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QRcode extends Model
{
    protected $table = 'qr';

    protected $fillable = [
        'qr_code','status','reservation_id','reservation_code'
    ];

    public function reservation()
    {
        return $this->hasOne('App\Reservation','id','reservation_id');
    }
}
