<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $table = 'credentials';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password','userStatus','userType','signal_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    public function userPreviledge()
    {
        return $this->hasOne('App\UserPreviledge','userId','id');
    }

    public function userDetails()
    {
        return $this->belongsTo('App\UserDetails','id','detailsId');
    }
}
