<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPreviledge extends Model
{
    protected $table = 'userpreviledge';

    protected $fillable = ['previledgeId','userId','parkingId','detailsId','companyId'];

    public function companyParking()
    {
        return $this->belongsTo('App\CompanyParking','parkingId','parkingId');
    }

    public function userDetails()
    {
        return $this->belongsTo('App\UserDetails','detailsId','detailsId');
    }

    public function Company()
    {
        return $this->hasOne('App\Company','companyId','companyId');
    }

    public function users()
    {
        return $this->hasMany('App\User','id','userId');
    }

    public function parkingDetails()
    {
        return $this->belongsTo('App\ParkingDetails','parkingId','parkingId');
    }
}
