<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = 'reservation';

    protected $fillable = [
        'start', 'end', 'plateNumber', 'brand', 'model', 'color', 'make', 'parkingId', 'user_id', 'date','reservation_fee'
    ];

    public function transaction()
    {
        return $this->belongsTo('App\Transaction','reservation_id','id');
    }

    public function transactionTo()
    {
        return $this->hasOne('App\Transaction','reservation_id','id');
    }
}
