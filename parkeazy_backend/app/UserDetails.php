<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    protected $table = 'userdetails';

    protected $fillable = ['detailsId','firstname','lastname','contact'];
}
