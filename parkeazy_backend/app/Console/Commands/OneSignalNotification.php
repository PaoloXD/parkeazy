<?php

namespace App\Console\Commands;

use App\Services\OneSignal;
use Illuminate\Console\Command;
use Carbon\Carbon;
use DB;

class OneSignalNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Onesignal:Notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = [
            Carbon::now('Asia/Manila')->addMinutes(13)->toTimeString(),
            Carbon::now('Asia/Manila')->addMinutes(15)->toTimeString()
        ];
        $date = Carbon::now('Asia/Manila')->toDateString();

        //echo Carbon::now('Asia/Manila')->toTimeString().'<br>'.$date;

        $users = DB::table('transaction')->select('transaction.*','reservation.*','credentials.*')
            ->where('transaction.actual_out','=',null)
            ->join('reservation','transaction.reservation_id','=','reservation.id')
            ->where('reservation.date','=',$date)
            ->whereBetween('reservation.end',$time)
            ->join('credentials','reservation.user_id','=','credentials.id')
            ->select('credentials.signal_id')
            ->get();

        if($users != null) {
            $signal_id_list = [];
            foreach ($users as $key => $user) {
                array_push($signal_id_list, $user->signal_id);
            }

            $parseFields = array(
                'app_id' => \Config::get('constant.app_id'),
                'include_player_ids' => $signal_id_list,
                'data' => array(),
                'contents' => array(
                    "en" => 'Your time is about to end.'
                )
            );
            //dd($parseFields);
            $encode_fields = json_encode($parseFields);

            OneSignal::PushNotification($encode_fields);

            $this->info('Onesignal:Notification Sending Cummand Run successfully! - '.Carbon::now('Asia/Manila')->addMinutes(15)->toTimeString());
        }

        $this->info('Onesignal:Notification Cummand Run successfully! - '.Carbon::now('Asia/Manila')->addMinutes(15)->toTimeString());
    }
}
