<?php

/**
 * Created by PhpStorm.
 * User: Paolo
 * Date: 8/28/2016
 * Time: 6:19 PM
 */

    namespace App\Services;

class OneSignal
{
    public static function PushNotification($fields)
    {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Basic '.\Config::get('constant.rest_key')
        ));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HEADER, false );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $fields );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );    # required for https urls
        $response = curl_exec( $ch );
        //$response = curl_getinfo( $ch );
        curl_close ( $ch );

        return $response;
    }
}