<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected  $table = 'transaction';

    protected $fillable = [
        'reservation_id','actual_in','actual_out','overtime','payment','cashier_id'
    ];

    public function reservation()
    {
        return $this->belongsTo('App\Reservation','reservation_id','id');
    }
}
