<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyParking extends Model
{
    protected $table = 'companyparking';

    protected $fillable = ['companyId','parkingId','parkingStatus'];

    public function company()
    {
        return $this->belongsTo('App\Company','companyId','companyId');
    }

    public function parkingDetail()
    {
        return $this->belongsTo('App\ParkingDetails','parkingId','parkingId');
    }

    public function parkingMap()
    {
        return $this->belongsTo('App\ParkingMap','parkingId','parkingId');
    }

    public function parkingRate()
    {
        return $this->belongsTo('App\ParkingRate','parkingId','parkingId');
    }

    public function reservation()
    {
        return $this->hasMany('App\Reservation','parkingId','parkingId');
    }
}
