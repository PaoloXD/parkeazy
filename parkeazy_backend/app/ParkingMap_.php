<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParkingMap extends Model
{
    protected $table = 'ez_parkingmap';

    protected $fillable = ['parkingId','lng','lat'];
}
