<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'company';

    protected $fillable = ['companyName','companyAddress','companyStatus'];

    public function companyParking()
    {
        return $this->hasMany('App\CompanyParking','companyId','companyId');
    }
}
