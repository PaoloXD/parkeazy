<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParkingRate extends Model
{
    protected $table = 'parkingrate';

    protected $fillable = [
        'parkingId','ratePerHour','minimumRate','openHrs','closeHrs'
    ];
}
