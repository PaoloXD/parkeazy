<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParkingDetails extends Model
{
    protected $table = 'parkingdetails';

    protected $fillable = [
        'parkingId','parkingName','address','numberOfSlots','availableSlots','lng','lat','reservation_amount','reservation_min_time','succeeding_amount','succeeding_time','amount_per_hour'
    ];

    public function parkingMap()
    {
        return $this->belongsTo('App\ParkingMap','parkingId','parkingId');
    }
}
