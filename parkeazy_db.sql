/*
Navicat MariaDB Data Transfer

Source Server         : localhost
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : parkeazy_db

Target Server Type    : MariaDB
Target Server Version : 100113
File Encoding         : 65001

Date: 2016-06-28 01:47:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ez_company
-- ----------------------------
DROP TABLE IF EXISTS `ez_company`;
CREATE TABLE `ez_company` (
  `companyId` int(11) NOT NULL AUTO_INCREMENT,
  `companyName` varchar(255) NOT NULL,
  `companyAddress` varchar(255) NOT NULL,
  `companyStatus` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`companyId`),
  KEY `companyId` (`companyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ez_company
-- ----------------------------

-- ----------------------------
-- Table structure for ez_companyparking
-- ----------------------------
DROP TABLE IF EXISTS `ez_companyparking`;
CREATE TABLE `ez_companyparking` (
  `companyId` int(11) NOT NULL,
  `parkingId` int(11) NOT NULL AUTO_INCREMENT,
  `parkingStatus` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`parkingId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ez_companyparking
-- ----------------------------

-- ----------------------------
-- Table structure for ez_credentials
-- ----------------------------
DROP TABLE IF EXISTS `ez_credentials`;
CREATE TABLE `ez_credentials` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `userStatus` int(11) NOT NULL DEFAULT '0',
  `userType` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ez_credentials
-- ----------------------------
INSERT INTO `ez_credentials` VALUES ('1', 'admin@park.com', '$2y$10$ZiJg1VUqikpGsWgRl6BjXO82opHRrOTU67CLNCItdUmhK2jF5ZGdG', '0', '0', '2016-06-25 00:00:00', '2016-06-25 00:00:00');

-- ----------------------------
-- Table structure for ez_parkingdetails
-- ----------------------------
DROP TABLE IF EXISTS `ez_parkingdetails`;
CREATE TABLE `ez_parkingdetails` (
  `parkingId` int(11) NOT NULL,
  `parkingName` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `numberOfSlots` int(11) NOT NULL,
  `availableSlots` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  UNIQUE KEY `parkingId_2` (`parkingId`),
  KEY `parkingId` (`parkingId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ez_parkingdetails
-- ----------------------------

-- ----------------------------
-- Table structure for ez_parkingmap
-- ----------------------------
DROP TABLE IF EXISTS `ez_parkingmap`;
CREATE TABLE `ez_parkingmap` (
  `parkingId` int(11) NOT NULL,
  `lng` double NOT NULL,
  `lat` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`parkingId`),
  KEY `parkingId` (`parkingId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ez_parkingmap
-- ----------------------------

-- ----------------------------
-- Table structure for ez_parkingrate
-- ----------------------------
DROP TABLE IF EXISTS `ez_parkingrate`;
CREATE TABLE `ez_parkingrate` (
  `parkingId` int(11) NOT NULL,
  `ratePerHour` int(11) NOT NULL,
  `minimumRate` int(11) NOT NULL,
  `openHrs` varchar(30) NOT NULL,
  `closeHrs` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  KEY `parkingId` (`parkingId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ez_parkingrate
-- ----------------------------

-- ----------------------------
-- Table structure for ez_userdetails
-- ----------------------------
DROP TABLE IF EXISTS `ez_userdetails`;
CREATE TABLE `ez_userdetails` (
  `detailsId` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  KEY `detailsId` (`detailsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ez_userdetails
-- ----------------------------

-- ----------------------------
-- Table structure for ez_userpreviledge
-- ----------------------------
DROP TABLE IF EXISTS `ez_userpreviledge`;
CREATE TABLE `ez_userpreviledge` (
  `previledgeId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `parkingId` int(11) NOT NULL,
  `companyId` int(11) NOT NULL,
  `detailsId` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`previledgeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ez_userpreviledge
-- ----------------------------

-- ----------------------------
-- Table structure for ez_usertype
-- ----------------------------
DROP TABLE IF EXISTS `ez_usertype`;
CREATE TABLE `ez_usertype` (
  `Administrator` int(11) NOT NULL DEFAULT '0',
  `Supervisor` int(11) NOT NULL DEFAULT '1',
  `Cashier` int(11) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ez_usertype
-- ----------------------------
